module.exports = {
  eslintIntegration: true,
  printWidth: 100,
  tabWidth: 2,
  useTabs: false,
  semi: true,
  vueIndentScriptAndStyle: true,
  singleQuote: true,
  quoteProps: 'as-needed',
  // 对象间的空格
  bracketSpacing: false,
  trailingComma: 'none',
  arrowParens: 'avoid',
  insertPragma: false,
  requirePragma: false,
  proseWrap: 'never',
  // 行尾换行格式
  htmlWhitespaceSensitivity: 'css',
  endOfLine: 'lf'
  // embeddedLanguageFormatting: 'off'
};

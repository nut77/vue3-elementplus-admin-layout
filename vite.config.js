import {defineConfig, loadEnv} from 'vite';
import vue from '@vitejs/plugin-vue';
import {injectHtml} from 'vite-plugin-html';
const path = require('path');

const env = loadEnv('', process.cwd());
export default defineConfig({
  plugins: [
    vue(),
    injectHtml({
      injectData: {...env}
    })
  ],
  resolve: {
    extensions: ['.js', '.json', '.vue', '.mjs'],
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@a': path.resolve(__dirname, './src/assets'),
      '@p': path.resolve(__dirname, './src/pages'),
      '@c': path.resolve(__dirname, './src/composables'),
      '@l': path.resolve(__dirname, './src/layouts')
    }
  },
  server: {
    host: '0.0.0.0',
    port: '8000',
    fs: {
      strict: false
    },
    // open: true,
    proxy: {
      '/api': {
        target: 'http://api.ifbes.com/mock/20/api',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/api/, '')
      },
      '/download': {
        target: 'https://npm.taobao.org/mirrors/node',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/download/, '')
      }
    }
  },
  css: {
    preprocessorOptions: {
      less: {
        additionalData: `@import "@a/styles/variable.less";`
      }
    }
  }
});

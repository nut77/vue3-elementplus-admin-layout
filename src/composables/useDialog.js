import {ref} from 'vue';
export default function (proxy) {
  let dialogId = ref(0);
  const showDialog = () => dialogId.value = Date.now();
  const hideDialog = () => dialogId.value = 0;

  return {
    dialogId,
    showDialog,
    hideDialog
  }
}

import {ref} from 'vue';
export default function () {
  let drawerId = ref(0);
  const showDrawer = () => drawerId.value = Date.now();
  const hideDrawer = () => drawerId.value = 0;
  return {
    drawerId,
    showDrawer,
    hideDrawer
  }
}

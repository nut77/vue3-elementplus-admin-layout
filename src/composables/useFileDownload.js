import qs from 'qs';
import {ref} from 'vue';

export default function (proxy) {
  // 下载用到的 notification组件对象键值对
  const notifications = ref({});

  /**
   * 同步下载，利用浏览器自带的文件下载功能
   * @param {string} url - 接口地址
   * @param {Object} [params=null] - 下载参数，qs.stringify会将该对像转为形如：id=123&name=Ada
   * @param {string} [filename=''] - 下载文件名
   */
  const downloadFile = (url, params = null, filename = '') => {
    const el = document.createElement('a');
    el.style.display = 'none';
    document.body.appendChild(el);
    filename && el.setAttribute('download', filename);
    const queryStr = [qs.stringify(params)].map(item => item && `?${item}`)[0];
    el.href = proxy.$tool.getFullUrl(url + queryStr);
    el.click();
    document.body.removeChild(el);
  };

  /**
   * 异步-带header(token)
   * @param {string} [method='GET'] - 请求方法get/post(不区分大小写)
   * @param {string} url - 接口地址
   * @param {Object} params - 下载参数，get格式为：?id=123&&name="文件下载"
   * @param {string} filename - 下载文件名(必填，若为空，下载下来都是txt格式)
   * @param {Object} [config={}] - 异步请求其它配置
   */
  const downloadFileAsync = async (method = 'GET', url, params, filename, config = {}) => {
    const actionId = Date.now();
    const res = await proxy.$request.ajax(
      method,
      url,
      params,
      Object.assign(
        {
          baseURL: '/download',
          responseType: 'blob',
          transformResponse,
          onDownloadProgress: event => downloadProgress(event, actionId, filename)
        },
        config
      )
    );
    if (res.status === 200) {
      const fileReader = new FileReader();
      fileReader.readAsText(res.data);
      fileReader.onload = () => {
        try {
          // 考虑后端返回的不是文件流是错误描述对象的情况
          const jsonData = JSON.parse(fileReader.result);
          proxy.$message.error(jsonData.message);
        } catch (err) {
          const blob = new Blob([res.data]);
          const fileUrl = URL.createObjectURL(blob);
          downloadFile(fileUrl, null, filename || res.headers['content-disposition'] || '下载文件名');
          URL.revokeObjectURL(fileUrl);
          proxy.$message.success('文件已生成');
        }
      };
    } else {
      proxy.$message.error('文件获取失败，请稍后再试。');
    }
  };

  // 文件下载响应头格式
  const transformResponse = data => {
    return {
      data,
      status: data ? 200 : 500
    };
  };

  // 文件下载进度 actionId：时间戳-标识此次的下载事件
  const downloadProgress = ({loaded, total}, actionId, filename = '') => {
    const message = `${filename}<br/>${((loaded / total) * 100).toFixed(2)}%（${loaded} / ${total}）`;
    const className = `notification_${actionId}`;
    if (!notifications.value[actionId]) {
      notifications.value[actionId] = proxy.$notify.info({
        customClass: className,
        title: '文件生成进度',
        message,
        position: 'bottom-left',
        dangerouslyUseHTMLString: true,
        duration: 0
      });
    } else {
      document.querySelector(`.${className} .el-notification__content p`).innerHTML = message;
    }
    if (notifications.value[actionId] && total === loaded) notifications.value[actionId].close();
  };

  return {
    downloadFile,
    downloadFileAsync
  };
}
